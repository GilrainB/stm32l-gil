/**
*****************************************************************************
**
**  File        : main.c
**
**  Abstract    : main function.
**
**  Description : This project shows that the STM32L-DISCOVERY board is
**                capable of doing many things at a time while also letting
**                the user interact with the system.
**                The project uses LEDs connected to the STM32L.
**                The 8 LEDs that are connected could be shaped like a circle
**                and will form a pattern that changes over time
**                and reacts to input from the user.
**
**  Peripherals : UserBtn, LED's, timers, touch sensor
**
**  Environment : Atollic TrueSTUDIO/STM32
**                STMicroelectronics STM32Lxx Standard Peripherals Library
**
*****************************************************************************
*/

#include "main.h"

int main(void)
{
	RCC_Configuration();
	/* Init I/O ports */
	Init_GPIOs();

	/* Initializes the LCD glass */
	LCD_GLASS_Init();
	LCD_GLASS_DisplayString((uint8_t*) "  Vry"); // Vrije opdracht

	Init_NVIC();

	/* Switch on all leds at start and keep refreshing at predefined rate */
	Init_Timer (REFRESH_TIMER, REFRESH_FREQUENCY, 0);
	time_frame = 0;
	Timer_Start(REFRESH_TIMER);


	while ((GPIOA->IDR & USER_GPIO_PIN) == 0x0)	; // Wait for press of button

	/* Switch off the leds before program */
	FOR_ALL_LEDS	LEDs[i].state = 0 ;

	while ((GPIOA->IDR & USER_GPIO_PIN) != 0x0)	; // Release button


	/* Init Systick */
	Config_Systick();

	/* Configure the button and its timer */
	Init_GPIOA0_Interrupt();

	Init_Timer (BUTTON_TIMER, TIME_1MS, TIME_MS_BUTTON);
	Timer_Stop (BUTTON_TIMER); // Reset the timer counter

	volgendeMode();

	/* Infinite loop */
	while (1) {
		asm(" WFI");
	}
	return 0;
}

 ///////////////
 // Interrupt //
 // Functions //
 ///////////////

/**
 * Interrupt handler for systick in this program
 */
void SysTick_interrupt() {
	// Initialize the timer to 0
	static volatile int32_t timer30Sec = 0;

	if ((GPIOA->IDR & USER_GPIO_PIN) != 0x0) {
		// If the button was pressed, reset the timer
		timer30secReset();

	} else {
		timer30Sec++;

		if (timer30Sec >= SYSTICK_COUNT_30SEC) {
			timer30secReset();
			// Go to the next function
			volgendeMode();
		}
	}
}

/**
 * Interrupt handler for USER button(GPIO A0)
 */
void EXTI0_IRQHandler(void) {
	/* Make sure that interrupt flag is set */
	if (EXTI_GetITStatus(EXTI_Line0) == SET) {

		if(GPIOA->IDR & GPIO_Pin_0){
			/* Button was pressed */

			// Enable the timer
			Timer_Start(BUTTON_TIMER);

		} else {
			/* Button was released */

			// Disable the timer
			Timer_Stop(BUTTON_TIMER);
		}


		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

/**
 * Interrupt handler for timer 2
 * Timer 2 is the timer for PWM.
 * Recurrence: REFRESH_FREQUENCY
 */
void TIM2_IRQHandler() {
	if (TIM_GetITStatus(REFRESH_TIMER, TIM_IT_Update) == SET) {

		time_frame %= REFRESH_FREQUENCY; // If not using PWM, this depicts the time

		if (doFunctions != NULL) {
			doFunctions(time_frame);
			time_frame++;
		}
		refreshLEDs();

		/* Clear interrupt flag */
		TIM_ClearITPendingBit(REFRESH_TIMER, TIM_IT_Update);
	}
}

/**
 * Interrupt handler for timer 3
 * Timer 3 is the timer used to measure if the user has pressed the user button long enough
 */
void TIM3_IRQHandler() {
	static uint8_t first_time = 1;
	if (first_time){
		// Rerun timer
		Timer_Stop(BUTTON_TIMER);
		/* Clear interrupt flag */
		TIM_ClearITPendingBit(BUTTON_TIMER, TIM_IT_Update);
		Timer_Start(BUTTON_TIMER);
		first_time = 0;
		return;

	}
	if (TIM_GetITStatus(BUTTON_TIMER, TIM_IT_Update) == SET) {

		volgendeMode();

		/* Clear interrupt flag */
		TIM_ClearITPendingBit(BUTTON_TIMER, TIM_IT_Update);

		Timer_Stop(BUTTON_TIMER);
	}
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/*
 * Minimal __assert_func used by the assert() macro
 * */
void __assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
  while(1)
  {}
}

/*
 * Minimal __assert() uses __assert__func()
 * */
void __assert(const char *file, int line, const char *failedexpr)
{
   __assert_func (file, line, NULL, failedexpr);
}

#ifdef USE_SEE
#ifndef USE_DEFAULT_TIMEOUT_CALLBACK
/**
  * @brief  Basic management of the timeout situation.
  * @param  None.
  * @retval sEE_FAIL.
  */
uint32_t sEE_TIMEOUT_UserCallback(void)
{
  /* Return with error code */
  return sEE_FAIL;
}
#endif
#endif /* USE_SEE */

 ///////////////////
 // Configuration //
 ///  Functions  ///
 ///////////////////

/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */
void RCC_Configuration(void)
{
  /* Enable the GPIOs Clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC| RCC_AHBPeriph_GPIOD| RCC_AHBPeriph_GPIOE| RCC_AHBPeriph_GPIOH, ENABLE);

  /* Enable comparator clock, LCD and Timers */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_COMP | RCC_APB1Periph_LCD | RCC_APB1Periph_PWR | RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3, ENABLE);

  /* Enable SYSCFG */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Allow access to the RTC */
  PWR_RTCAccessCmd(ENABLE);

  /* Reset Backup Domain */
  RCC_RTCResetCmd(ENABLE);
  RCC_RTCResetCmd(DISABLE);

  /*!< LSE Enable */
  RCC_LSEConfig(RCC_LSE_ON);

  /*!< Wait till LSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {}

  /*!< LCD Clock Source Selection */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
}


/**
  * @brief  To initialize the I/O ports
  * @caller main
  * @param None
  * @retval None
  */
void  Init_GPIOs (void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure User Button pin as input */
  GPIO_InitStructure.GPIO_Pin = USER_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(BUTTON_GPIO_PORT, &GPIO_InitStructure);

  initLEDs();
/* Configure the LEDs pins*/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

  // Port A
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_15;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  // Port B
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  // Port C
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_12;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

/* Configure Output for LCD */
/* Port A */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_8 | GPIO_Pin_9 |GPIO_Pin_10 /*|GPIO_Pin_15*/;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init( GPIOA, &GPIO_InitStructure);

  GPIO_PinAFConfig(GPIOA, GPIO_PinSource1,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource8,GPIO_AF_LCD) ; // Cruciaal
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9,GPIO_AF_LCD) ; // Cruciaal
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10,GPIO_AF_LCD) ; // Cruciaal
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource15,GPIO_AF_LCD) ; // Inactief: Rechtsboven char 6

/* Configure Output for LCD */
/* Port B */
  GPIO_InitStructure.GPIO_Pin = /*GPIO_Pin_3 |*/ GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_8 | GPIO_Pin_9 \
                                 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init( GPIOB, &GPIO_InitStructure);

#ifdef USE_ALL_LCD_SEGMENTS
/**
 * Note!
 * PB3 is connected to C, M, COLON, and DP segments for the second digit on the LCD
 * PB3 is also the SWO pin used for the Serial Wire Viewer (SWV)
 * If PB3 is used by LCD then SWV will not work for the STM32L_DISCOVERY board
 **/
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource3,GPIO_AF_LCD) ;
#endif
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource4,GPIO_AF_LCD) ; // Cruciaal
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5,GPIO_AF_LCD) ; // Cruciaal
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8,GPIO_AF_LCD) ; // Niet benodigd: linker kant char 6
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9,GPIO_AF_LCD) ; // Cruciaal
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource12,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource14,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource15,GPIO_AF_LCD) ;

/* Configure Output for LCD */
/* Port C*/
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 \
                                 | GPIO_Pin_7 | GPIO_Pin_8 /*| GPIO_Pin_9*/ | GPIO_Pin_10 |GPIO_Pin_11 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init( GPIOC, &GPIO_InitStructure);


  GPIO_PinAFConfig(GPIOC, GPIO_PinSource0,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource1,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource2,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource3,GPIO_AF_LCD) ;
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6,GPIO_AF_LCD) ; // Rechtsboven char 3
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7,GPIO_AF_LCD) ; // Linkerkant char 3
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource8,GPIO_AF_LCD) ; // Cruciaal voor char 2
  //GPIO_PinAFConfig(GPIOC, GPIO_PinSource9,GPIO_AF_LCD) ; // Inactief: Bovenkant char6, linksboven segmenten char 2
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource10,GPIO_AF_LCD) ; // Rechtsboven char 1
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource11,GPIO_AF_LCD) ; // Linkerkant char 1
}


void Config_Systick()
{
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(SYSTICK_TIME);
}


void Init_GPIOA0_Interrupt() {
	// Configure the interrupt(as instructed on line 29 of stm32l1xx_exti.c
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

	EXTI_InitTypeDef exti;
	exti.EXTI_Line = EXTI_Line0;
	exti.EXTI_LineCmd = ENABLE;
	exti.EXTI_Mode = EXTI_Mode_Interrupt;
	exti.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_Init(&exti); // Pins 0 will generate an EXT(ernal) Interrupt on line 0

}


void Init_Timer(TIM_TypeDef * timer, uint32_t freq, uint16_t multiplier){
	// Configuring timer in Timing(Time base) Mode as instructed from line 156 of stm32l1xx_tim.c

	// Step 1: Timers must already have been enabled using the RCC!

	TIM_UpdateDisableConfig(timer, ENABLE); // First step: Disable any counter interrupt

	// Step 2: Timer trigger time configuration
	RCC_GetClocksFreq(&RCC_Clocks);
	uint32_t freqInternal = RCC_Clocks.SYSCLK_Frequency; // Internal/system frequency
	TIM_TimeBaseInitTypeDef initStruct;
	TIM_TimeBaseStructInit(&initStruct);

	/**
	 * Note that the timer might get its clock source from the internal clock
	 * The counter clock frequency CK_CNT is equal to ( (freq_CK_PSC) / (PSC[15:0] + 1) ).

	 * When trying to set up the timer to generate an interrupt at regular rates 'a second,
	 * we would want to divide the total time period into more smaller ones,
	 * for example 1000 to indicate a pro-mille like something.
	 * For example if f=1000Hz, this would mean TIM_Prescaler = 1ms, so TIM_Period would count to 1000.
	 * if f=500, then prescaler must be 2ms to have the same rule
	 * if f=500 and we would want to have less time periods, we would for example use:
	 *      prescaler = 1s/50 causing the period to be 10=(500/50)
	 * It should never be forgotten that the prescaler can only count to 2^16
	 * and that the timer counter will count to 2^32
	 */

	/**
	 * How does it calculate the optimal prescaler and timer counter?
	 * Rule 1: sysclk / freq ~= presc * count ;
	 *         system clock / frequency ~= time period ~= prescale-time * timer counter
	 *
	 * So first calculate the time period of the frequency
	 * period = sysclk / freq
	 *
	 * Strategy 1: Check how many bits too much there are for the period to be entered into the timer
	 *    Typically there would be enough of them, as we are talking about a frequency; amounts per second.
	 *    If that would not be true, you would compensate for it inside the timer ISR itself.
	 *
	 * Strategy 2: Try to fill the time into the prescaler(making small time segments)
	 *
	 * Strategy 3: Use a standard amount of counting segments(presc)
	 *   Implies that the amount of times to count to (presc) needs to be determined.
	 *   This method may be very bad in terms of precision; the actual time period would be off by
	 *   this amount: request period % prescaler tick count . And this offset may be as much ticks as
	 *   prescaler tick count - 1  -- Which would not be acceptable
	 *
	 * Strategy 4: Use a standard amount of time segments(count) -- best option yet simple
	 *   Implies that a prescaler is calculated so the timer would always count to the same number(s).
	 *   Precision: maximum  of 2^32 time periods in stead of 2^16
	 *
	 * Strategy 5: Assume the amount of ticks fits in a 32-bit register
	 *   Always count to the amount of ticks needed, without modifying the prescaler.
	 */

	if (multiplier == 0) {
		volatile uint32_t period = freqInternal / freq; // Amount of clock ticks to target time(total time)

		uint32_t i = 0;
		while ((period>>i) & ~0xFFFF) i++;
		/*
		 // Using strategy 2(inversed) and 3.
		 // if period > 0x=2^16, Counter is
		 // 		0x40 = 64 =2^6
		 // else 0x1  =  1 =2^0 otherwise

		 uint16_t presc = (period < 0x10000)? 0x1: 0x40; // Splitting up into this amount of periods, more precise if it's a small value
		 uint32_t cnt = period / presc; // One time segment is this amount of ticks; timer is incremented after <presc> ticks
		 */

		initStruct.TIM_Prescaler = i; // Period time (minus 1 because prescaler == presc_register + 1)
		initStruct.TIM_Period = period >> i; // Amount of times the timer must have "ticked" until one time period has passed

	} else {
		// If multiplier is not zero, ticks * multiplier will be the period of this timer
		// For example Init_Timer(TIM3, <millisec>,	TIME_1MS)
		initStruct.TIM_Prescaler = multiplier;
		initStruct.TIM_Period = freq; // time for 1 millisec in ticks from the system clock(for example)
	}
	TIM_TimeBaseInit(timer, &initStruct);

	// Step 3: NVIC configuration
	// Assume the NVIC has been enabled already

	// Configure the timer to cause an interrupt
	TIM_ITConfig(timer, TIM_IT_Update, ENABLE);

	/* Clear interrupt flag */
	TIM_ClearITPendingBit(BUTTON_TIMER, TIM_IT_Update);

	TIM_UpdateDisableConfig(timer, DISABLE);// Last step: Enable the interrupts

	// TIM_Cmd(timer, ENABLE);
}


void Init_NVIC(){
	// Configure NVIC IRQ channel mapped to the timers and button using NVIC_Init()

	// Button A0
		NVIC_InitTypeDef nvic;
		nvic.NVIC_IRQChannel = EXTI0_IRQn;
		nvic.NVIC_IRQChannelCmd = ENABLE;
		nvic.NVIC_IRQChannelPreemptionPriority = 0;
		nvic.NVIC_IRQChannelSubPriority = 0;
		NVIC_Init(&nvic);

		// Timer 2 for refreshing
		nvic.NVIC_IRQChannel = TIM2_IRQn;
		NVIC_Init(&nvic);

		// Timer 3 for pressing the button
		nvic.NVIC_IRQChannel = TIM3_IRQn;
		NVIC_Init(&nvic);
}


 ///////////////
 ///   Wait  ///
 // Functions //
 ///////////////

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in 10 ms.
  * @retval None
  */
void Delay(uint32_t nTime)
{
  TimingDelay = nTime;

  while(TimingDelay != 0)asm(" WFI");

}


/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{

  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
}

 ///////////////////
 // LED functions //
 ///////////////////

void initLEDs() {

	FOR_ALL_LEDS
		LEDs[i].state = 1; // Turn on all LEDs

	LEDs[0].port = LED0_PORT;
	LEDs[1].port = LED1_PORT;
	LEDs[2].port = LED2_PORT;
	LEDs[3].port = LED3_PORT;
	LEDs[4].port = LED4_PORT;
	LEDs[5].port = LED5_PORT;
	LEDs[6].port = LED6_PORT;
	LEDs[7].port = LED7_PORT;

	LEDs[0].pin = LED0_PIN;
	LEDs[1].pin = LED1_PIN;
	LEDs[2].pin = LED2_PIN;
	LEDs[3].pin = LED3_PIN;
	LEDs[4].pin = LED4_PIN;
	LEDs[5].pin = LED5_PIN;
	LEDs[6].pin = LED6_PIN;
	LEDs[7].pin = LED7_PIN;

	LEDs[0].colour = LED0_COLOUR;
	LEDs[1].colour = LED1_COLOUR;
	LEDs[2].colour = LED2_COLOUR;
	LEDs[3].colour = LED3_COLOUR;
	LEDs[4].colour = LED4_COLOUR;
	LEDs[5].colour = LED5_COLOUR;
	LEDs[6].colour = LED6_COLOUR;
	LEDs[7].colour = LED7_COLOUR;

	FOR_ALL_LEDS
		LEDs[i].multi = LEDs[i].period = LEDs[i].time = 0;
}


void refreshLEDs(){

	FOR_ALL_LEDS
		if(LEDs[i].state)
			LEDs[i].port->BSRRL = LEDs[i].pin; // Set bit
		else
			LEDs[i].port->BSRRH = LEDs[i].pin; // Clear bit
}


 ///////////////
 ///  Modes' ///
 // Functions //
 ///////////////

uint32_t modeFuncVar1 = 0;
uint32_t modeFuncVar2 = 0;

void volgendeMode() {
#define caseFunc(a) if (doFunctions == (a))
#define elseFunc(a) else caseFunc(a)

	time_frame = 0; // Reset counting

	caseFunc(mode1_AanUit1Hz){
		modeFuncVar1 = 0; // Current position
		modeFuncVar2 = -1;// Previous position
		doFunctions = mode2_Draai;
	}

	elseFunc(mode2_Draai){
		modeFuncVar1 = 1;	// Count up
		modeFuncVar2 = 1;		// From 1(=12,5%) up to 8(=100%)
		doFunctions = mode5_Pulseren; // mode3_Draai_Fade;
	}
/*
	elseFunc(mode3_Draai_Fade){
		modeFuncVar1 = 0;
		doFunctions = mode1_AanUit1Hz; // mode4_Fade_Groepen;
	}

	elseFunc(mode4_Fade_Groepen){
		doFunctions = mode3_Draai_Fade; // mode5_Pulseren;
	}

	elseFunc(mode5_Pulseren){
		doFunctions = mode6_Touch;
	}

	elseFunc(mode6_Touch){ // Delete this instance when all functions are finished
		doFunctions = mode4_Fade_Groepen;
	}
*/
	else { // ook voor niet-ge�mplementeerde functies
		modeFuncVar1 = 0;
		doFunctions = mode1_AanUit1Hz;
	}

	if (doFunctions == NULL)
		return volgendeMode();

#undef caseFunc
#undef elseFunc
}

void PWM_Optimize(LED *led){
	uint16_t result1 = led->period % led->multi;
	if(result1 == 0){
		// multiplier fits in period(equal parts)
		led->period /= led->multi ;
		led->multi = 1;
		// return true;
	} else {
		// Actual simplification of the fraction ( division by 16-2 )
		uint16_t div = 16;
		for (; (div>=2) && !(led->period % div == 0 && led->multi % div == 0); div--);
		
		led->period /= div ;
		led->multi  /= div ;
		// return div>1 ;
	}
}


bool pwm_function(LED *led){
	if ((led->period || led->multi) == 0)
		return led->state; // Keep the previous state

	// Just check if the counter's up, if it's not, continue the program
	// If it is, turn on the led for this period
	// If period is 1, sequence will be 01010101, 1 in 2, aka 50%
	// If period is 3, sequence will be 00010001, 1 in 4, aka 25%
	// If period is 7, sequence will be 00000001, 1 in 8, aka 12,5%
	// If period is 9, sequence will be 0000000001, 1/10, aka 10%
	// If I do not want to see 10%, then FPS must be 10*25(eyes) = 250FPS
	// If I don't want to see 10% in the corner of my eye, 10*30+(?) = 300FPS

	if (led->time >= led->period) { // When time's up and increment after check
		led->time = 0;
		return led->state = ENABLE; // Enable LED
	} else if (led->time++ == led->multi -1) // If multiplier was 1, disable on time 0
		return led->state = DISABLE;
	else
		return led->state; // LED already is at state DISABLE



	/**** Old strategies: ****/
	// Promillage: values from 1 to 1000 are valid and if currentTime is 0, always return TRUE
	//if (promillage >= 1000 || currentTime == 0)
		//return TRUE;

	// For REFRESH_FREQUENCY==250, minimum promillage = 4(=1000/250)
	//if (promillage <= (1000 / REFRESH_FREQUENCY))
		//return FALSE; // never ON

	// For example 10% on a scale of 200:
	//  ( 200 * 100 ) / 1000 = 20000 / 1000 = 20 ticks -- without losing precision
	// Furthermore, 20 would then be used as a divisor
	// If you would divide currentTime in 20, if the result is even(or zero)
	//  then return TRUE, otherwise FALSE

	/////////
	// max(promille)/promillage = aantal x dat output TRUE moet zijn in max' tijddomein: <totaal>
	// dus als cT % <totaal> == 0, dan is cT een getal waarop de output TRUE moet zijn
	// Dus als remainder==0, dan return TRUE, anders FALSE
	// vb. cT=1, promillage=8, max=250(voorspeld FALSE), dan <totaal>=250/8
	// vb. cT=1, promillage=10
	/////////

	// YourMaximum / MyMaximum = factor; my maximum is factor amount smaller than yours
	//        1000 / 250       = 4; Every 4 of your max will fit in 1 of mine
	//  YourAmount / factor    = MyAmount; If you wanted the answer to be true x times, it's y times in my time region
	//      MyTime % (MyMaximum / myAmount)  == 0; If I divide my time y periods and no remainder exists, turn on my LED
	// Proof of concept:
	// yourmax = 1000, mine = 250, yourPeriod = 8(promille), MyTime= 1(cT), prediction = off
	// factor = 1000/250, MyAmount = 8/4 = 2 times, on? = 1 % (250/2) == 0?: off
	// Proof of concept:
	// yourmax = 1000, mine = 250, yourPeriod = 8(promille), MyTime= 5(cT), prediction = off
	// factor = 1000/250, MyAmount = 8/4 = 2 times, on? = 5 % (250/2) == 0?: off
	//return ( currentTime % (  maximum / ( promillage / (1000 / REFRESH_FREQUENCY) )  ) ) == 0; // needs replacing; not precise enough

	// FAULTY:
	// yourmax = 1000, mine = 250, yourPeriod = 8(promille), MyTime= 1(cT), prediction = off
	// factor = 1000/250 = 4, myperiod = 8/4 = 2, on? = 1 % 2 = off
	// yourmax = 1000, mine = 250, yourPeriod = 8(promille), MyTime= 2(cT), prediction = off
	// factor = 1000/250 = 4, myperiod = 8/4 = 2, on? = 2 % 2 = on


	//uint32_t ticks = (maximum * promillage) / 1000; // The amount of cycles(in currentTime domain) that this function must answer TRUE, the rest = false
	// these cycles(ticks) have to be distributed over <maximum>, so maximum/ticks = when to toggle the bit
	/* if (ticks == 0)
		return FALSE; // Too low promillage */

	//uint32_t mod = maximum / ticks; // Every cT must be % with mod. If remainder = 0 or cT=0, then ans = true

	//return ((currentTime % mod) != 0) ? FALSE : TRUE;
	//return (currentTime / ticks) ^ 1;
	// cT = max-1, pro=999, ans=0 ; cT=0 t/m 998, pro=999, ans=1; formule= (cT%pro > 0)

}


void mode1_AanUit1Hz	(int time){ // Prioriteit: 1

	// modeFuncVar1 = Used to remember if we turned on or off during the previous 'time == 0'-occurrence

	if (time % (REFRESH_FREQUENCY/2) == 0){
		if (modeFuncVar1)
			modeFuncVar1 = 0;
		else
			modeFuncVar1 = 1;

		FOR_ALL_LEDS
			LEDs[i].state = modeFuncVar1;
	}
	// After this function has been executed(and all other mode*()'s too) the software will refresh the LEDs
}


void mode2_Draai		(int time){ // Prioriteit: 2
	switch (time) {
	case 0:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 0) ? ENABLE : DISABLE;
		break;
	case (1 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 1) ? ENABLE : DISABLE;
		break;
	case (2 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 2) ? ENABLE : DISABLE;
		break;
	case (3 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 3) ? ENABLE : DISABLE;
		break;
	case (4 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 4) ? ENABLE : DISABLE;
		break;
	case (5 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 5) ? ENABLE : DISABLE;
		break;
	case (6 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 6) ? ENABLE : DISABLE;
		break;
	case (7 * REFRESH_FREQUENCY) / 8:
		FOR_ALL_LEDS
			LEDs[i].state = (i == 7) ? ENABLE : DISABLE;
		break;
	//default:
		// Impossible: if( time > REFRESH_FREQUENCY ) /* Error */; // time is never REFRESH_FREQUENCY
		// So do nothing
	}
}
/*
void mode3_Draai_Fade	(int time) ; // Prioriteit: 6
void mode4_Fade_Groepen	(int time) ; // Prioriteit: 5
*/

void mode5_Pulseren		(int time) { // Prioriteit: 3
	// time: current time(resets after 0,25 second), which marks a new pulse width modulation
	// modeFuncVar1 = Direction (1/true is up, 0/false is down)
	// modeFuncVar2 = Current pulse width(high part of the fraction)
	// modeFuncVar3 = Pulse width lower part of the fraction(with 8, animation is 8 secs from 1 to max)(IS A CONSTANT)

	if (time >= REFRESH_FREQUENCY/64 ) // Every 64th second, change
		time_frame = 0 ;

#define modeFuncVar3 32
	if (time_frame == 0) {
		LED *led = LEDs;
		led->period = modeFuncVar3;
		led->multi = modeFuncVar2;
		// Smoothen all transitions out
		led->time = 0;
		led->state = ENABLE;
		//PWM_Optimize(LEDs); // LED[0] ptr

		if (modeFuncVar2 >= modeFuncVar3)
			modeFuncVar1 = 0;	// Count down from here
		else if (modeFuncVar2 == 1)
			modeFuncVar1 = 1;	// Count up from here

		if (modeFuncVar1)
			modeFuncVar2++;
		else
			modeFuncVar2--;
	}
#undef modeFuncVar3

	bool state = pwm_function(LEDs);


	FOR_ALL_LEDS
		LEDs[i].state = state;
}

/*
void mode6_Touch		(int time) ; // Prioriteit: 4
*/

