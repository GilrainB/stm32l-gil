/*
 *
 */

#ifndef MAIN_H_
#define MAIN_H_

/* Includes */
#include <stddef.h>
#include "stm32l1xx.h"
#include "misc.h"
#include "stm32l1xx_exti.h"
#include "stm32l1xx_tim.h"
#include "stm32l1xx_rcc.h"

#include "stm32l_discovery_lcd.h"


// Generic function prototypes
void Init_GPIOs(void);
void Init_GPIOA0_Interrupt();
void Init_Timer(TIM_TypeDef*, uint32_t, uint16_t);
void Init_NVIC();
void RCC_Configuration(void);
void Config_Systick(void);

// Generic global variables
RCC_ClocksTypeDef RCC_Clocks; // Configured during Config_Systick()
#define BUTTON_GPIO_PORT	GPIOA
#define USER_GPIO_PIN		GPIO_Pin_0
#define REFRESH_TIMER		TIM2
#define BUTTON_TIMER		TIM3
#define TIME_1MS			(RCC_Clocks.SYSCLK_Frequency / 1000)
#define TIME_MS_BUTTON		(600)
#define SYSTICK_FREQUENCY 	(1000)
#define SYSTICK_TIME 		RCC_Clocks.HCLK_Frequency / SYSTICK_FREQUENCY
#define SYSTICK_COUNT_30SEC SYSTICK_FREQUENCY * 12
#define timer30secReset()   timer30Sec = 0

//////////////////////////////////
// Definitions/declarations for //
//       specific functions     //
//////////////////////////////////

/**
 * Tests gedaan bij 5% PWM
 * Min: 20x(glitter-ig bij beweging vanaf 24, extreem bij 16, flikkerend bij 14/15),
 * Max: 810x = ( max getest = 48600fps, O2)
 *
 * 47700fps(=265*180) werkt half; alleen bij O2 optimalisatie, niet bij optimized for debug)
 * Getest tot 48600 fps(werkte niet bij optimized for debug, wel bij O2)
 */
#define REFRESH_FREQUENCY (60*20*5) // 60 fps & per frame 100x = 6000fps om 10% netjes weer te kunnen geven

// Information for the LEDs
typedef enum { RED, BLUE, GREEN, YELLOW } LED_colour;

typedef struct {
	FunctionalState state;	/// Reset state is on
	uint16_t        period;	/// When using PWM, the period time to toggle the LED
	uint16_t        multi;  /// When using PWM, Multiplier; how many times per period must it be on(if not once)
	uint16_t        time;   /// When using PWM, counter
	GPIO_TypeDef *  port;   /// GPIO port for this LED
	uint16_t        pin;    /// GPIO pin for this LED
	LED_colour      colour; /// Colour of this LED
} LED;
/*** When using PWM,
 * use a period of 100 for a percentage(easy use)
 * Use a period of 256 for an Arduino-PWM
 * Use a (roundable) fraction(such as 39/60 for 65%)(or 1/4 for 25%)
 * Use the function and it checks if the fraction could be more compact( for instance entering 15/45 will result in 1/3)
 */
void PWM_Optimize(LED*);

LED LEDs[8];
void initLEDs();
void refreshLEDs();

// Define the used LEDs
#define LED0_PORT	GPIOB
#define LED0_PIN	GPIO_Pin_6
#define LED0_COLOUR	BLUE
#define LED1_PORT	GPIOB
#define LED1_PIN	GPIO_Pin_3
#define LED1_COLOUR	BLUE

#define LED2_PORT	GPIOC
#define LED2_PIN	GPIO_Pin_12
#define LED2_COLOUR	RED
#define LED3_PORT	GPIOA
#define LED3_PIN	GPIO_Pin_15
#define LED3_COLOUR	RED

#define LED4_PORT	GPIOA
#define LED4_PIN	GPIO_Pin_12
#define LED4_COLOUR	YELLOW
#define LED5_PORT	GPIOA
#define LED5_PIN	GPIO_Pin_11
#define LED5_COLOUR	YELLOW

#define LED6_PORT	GPIOC
#define LED6_PIN	GPIO_Pin_9
#define LED6_COLOUR	GREEN
#define LED7_PORT	GPIOB
#define LED7_PIN	GPIO_Pin_7
#define LED7_COLOUR	GREEN

#define FOR_ALL_LEDS for (int i = 0; i<8 ;i++)

// Define fake functions for turning on/off the button timer
#define Timer_Start(timer)	TIM_Cmd(timer, ENABLE)
#define Timer_Stop(timer)	{TIM_Cmd(timer, DISABLE); TIM_SetCounter(BUTTON_TIMER, 0);}

// Global variable for the time; it denotes the current frame in one second
int time_frame;

// Define all used functions
void (*doFunctions) (int time); // Function pointer where time is the current time (0 to REFRESH_FREQUENCY)

void volgendeMode();
bool pwm_function(LED*);

//void mode(int time); 				// Prototype
void mode1_AanUit1Hz	(int time); // Prioriteit: 1
void mode2_Draai		(int time); // Prioriteit: 2
void mode3_Draai_Fade	(int time); // Prioriteit: 6
void mode4_Fade_Groepen	(int time); // Prioriteit: 5
void mode5_Pulseren		(int time); // Prioriteit: 3
void mode6_Touch		(int time); // Prioriteit: 4

//////////////////////////
/// Interrupt handlers ///
//////////////////////////
void SysTick_interrupt();
void EXTI0_IRQHandler();
void TIM2_IRQHandler();
void TIM3_IRQHandler();

/////////////////////////
/// Library functions ///
///     for waiting   ///
/////////////////////////
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
void TimingDelay_Decrement(void);

#endif /* MAIN_H_ */
